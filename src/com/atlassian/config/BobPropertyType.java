package com.atlassian.config;

public interface BobPropertyType<T> {
    T parse(String stringValue);
}
