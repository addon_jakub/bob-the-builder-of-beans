package com.atlassian.config;

/**
 * Controls how BoB works at runtime
 */
public interface BobConfig {
    <T> T getProperty(BobProperty<T> property);
}
