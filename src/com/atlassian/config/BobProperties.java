package com.atlassian.config;

import static com.atlassian.config.BobProperty.property;
import static com.atlassian.config.BobPropertyTypes.BOOLEAN;
import static com.atlassian.config.BobPropertyTypes.INTEGER;

public final class BobProperties {
    public static final BobProperty<Integer> FIELD_COUNT_TO_REQUIRE_BUILDER = property("fieldCountToRequireBuilder", INTEGER, 4);
    public static final BobProperty<Boolean> GENERATE_COMMENT = property("generateComment", BOOLEAN, true);
}
