package com.atlassian.config;

import com.google.common.collect.Lists;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.vfs.VirtualFile;
import com.intellij.psi.*;

import java.io.IOException;
import java.util.List;
import java.util.Properties;

/**
 * Finds bob.properties relative to the class file passed in
 */
public class BobConfigFinder {

    public static BobConfig DEFAULT_CONFIG = new BobConfig() {
        @Override
        public <T> T getProperty(BobProperty<T> property) {
            return property.getDefaultValue();
        }
    };

    private static final String BOB_PROPERTIES_FILE_NAME = "bob.properties";


    public static BobConfig find(PsiClass psiClass) {
        PsiFile containingFile = psiClass.getContainingFile();

        if (!(containingFile instanceof PsiJavaFile)) {
            return DEFAULT_CONFIG;
        }

        List<VirtualFile> bobProperties = getBobPropertyFiles(psiClass.getProject(), (PsiJavaFile) containingFile);

        Properties properties = new Properties();
        for (VirtualFile bobProperty : bobProperties) {
            properties.putAll(toProperties(bobProperty));
        }

        return toBobConfig(properties);
    }

    /**
     * Returns a list of bob property files from least specific to most.
     */
    private static List<VirtualFile> getBobPropertyFiles(Project project, PsiJavaFile currentFile) {

        List<VirtualFile> result = Lists.newArrayList();

        JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);
        PsiPackage psiPackageTraverse = javaPsiFacade.findPackage(currentFile.getPackageName());

        while (psiPackageTraverse != null) {
            PsiDirectory[] packageDirectories = psiPackageTraverse.getDirectories();
            for (PsiDirectory packageDirectory : packageDirectories) {

                PsiFile bobProperties = packageDirectory.findFile(BOB_PROPERTIES_FILE_NAME);
                if (bobProperties != null) {
                    result.add(0, bobProperties.getVirtualFile());
                }
            }
            psiPackageTraverse = psiPackageTraverse.getParentPackage();
        }

        for (VirtualFile virtualFile : project.getBaseDir().getChildren()) {
            if (virtualFile.getName().equals(BOB_PROPERTIES_FILE_NAME)) {
                result.add(0, virtualFile);
            }
        }

        return result;
    }

    private static BobConfig toBobConfig(final Properties properties) {

        return new BobConfig() {
            @Override
            public <T> T getProperty(BobProperty<T> property) {
                String propertyString = properties.getProperty(property.getKey());
                try {
                    return propertyString != null ? property.getType().parse(propertyString) : property.getDefaultValue();
                } catch (RuntimeException ex) {
                    System.err.println(String.format("Could not parse property value '%s' of property '%s' (%s). Will use default value of %s",
                            propertyString, property.getKey(), ex.getMessage(), property.getDefaultValue()));
                    return property.getDefaultValue();
                }
            }
        };
    }

    private static Properties toProperties(VirtualFile bobProperties) {
        Properties configProperties = new Properties();
        try {
            configProperties.load(bobProperties.getInputStream());
            return configProperties;
        } catch (IOException e) {
            System.err.println("Error while reading properties from file " + bobProperties.getPath() + ": " + e.getMessage());
            return configProperties;
        }
    }
}
