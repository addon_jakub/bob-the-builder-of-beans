package com.atlassian.config;

public final class BobPropertyTypes {

    public static final BobPropertyType<Integer> INTEGER = Integer::valueOf;

    public static final BobPropertyType<Boolean> BOOLEAN = Boolean::valueOf;

}
