package com.atlassian.explore;

import com.intellij.openapi.project.Project;
import com.intellij.psi.*;

import java.io.IOException;
import java.util.Properties;

import static java.lang.String.format;
import static java.lang.System.out;

/**
 * Allows us to hack on idea and learn about the code model, since there isn't any public JavaDoc that I can find
 * Eventually this code can die once we (I) have a handle on the IDEA model
 */
public class ExploreIdea {

    public void hackOn(PsiClass psiClass) {
        Project project = psiClass.getProject();
        JavaPsiFacade javaPsiFacade = JavaPsiFacade.getInstance(project);

        PsiClass containingClass = psiClass.getContainingClass();
        PsiJavaFile javaFile = (PsiJavaFile) psiClass.getContainingFile();
        PsiPackage psiPackage = javaPsiFacade.findPackage(javaFile.getPackageName());
        PsiPackage psiPackageTraverse = psiPackage;
        while (psiPackageTraverse != null) {
            PsiDirectory[] packageDirectories = psiPackageTraverse.getDirectories();
            for (PsiDirectory packageDirectory : packageDirectories) {
                out.println(format("package '%s' in directory '%s'", psiPackageTraverse.getName(), packageDirectory.getName()));

                PsiFile bobProperties = packageDirectory.findFile("bob.properties");
                if (bobProperties != null) {

                    Properties properties = toProperties(bobProperties);
                    out.println(format("bob.properties class '%s'", bobProperties.getClass().getName()));
                }
            }
            psiPackageTraverse = psiPackageTraverse.getParentPackage();
        }


        visit(psiClass);
    }

    private Properties toProperties(PsiFile bobProperties) {
        Properties properties = new Properties();
        try {
            properties.load(bobProperties.getVirtualFile().getInputStream());
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
        return properties;
    }


    JavaElementVisitor myVisitor = new JavaElementVisitor() {
        @Override
        public void visitComment(PsiComment comment) {
            super.visitComment(comment);
            String text = comment.getText();
            out.println(format("visit comment '%s' : '%s'", comment.getClass().getName(), text));
        }
    };

    private void visit(PsiElement psiElement) {
        psiElement.accept(myVisitor);
        PsiElement element = psiElement.getFirstChild();
        while (element != null) {
            visit(element);
            element = element.getNextSibling();
        }
    }
}
