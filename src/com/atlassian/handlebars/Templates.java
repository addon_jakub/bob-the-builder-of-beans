package com.atlassian.handlebars;

import com.atlassian.util.JavaLang;
import com.atlassian.util.PluginUtils;
import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Helper;
import com.github.jknack.handlebars.Template;
import com.google.common.base.Strings;
import com.intellij.pom.java.LanguageLevel;

import java.io.IOException;

import static com.atlassian.action.BobTheBuilderAction.getEvent;
import static com.github.jknack.handlebars.Context.newContext;
import static com.intellij.pom.java.LanguageLevel.JDK_1_7;
import static com.intellij.pom.java.LanguageLevel.JDK_1_8;

public final class Templates {
    private static final Handlebars handlebars = new Handlebars();

    static {
        handlebars.registerHelper("startWithUpperCase", (Helper<String>) (context, options) -> startWithUpperCase(context));
        handlebars.registerHelper("singular", (Helper<String>) (context, options) -> singular(context));
        handlebars.registerHelper("upperSingular", (Helper<String>) (context, options) -> startWithUpperCase(singular(context)));
        handlebars.registerHelper("optionType", (Helper<String>) (context, options) -> GlobalContext.instance.getOptionalType() + "<" + context + ">");
        handlebars.registerHelper("createOption", (Helper<String>) (context, options) -> GlobalContext.instance.getOptionalConstructor() + "(" + context + ")");
        handlebars.registerHelper("genericClassCreation", (Helper<String>) (context, options) -> {
            boolean diamondOperatorSupport = getJavaVersion().isAtLeast(JDK_1_7);
            return diamondOperatorSupport && !Strings.isNullOrEmpty(context) ? "<>" : context;
        });
        handlebars.registerHelper("jsonProperty", (Helper<String>) (context, options) -> "@JsonProperty (\"" + context + "\")");
        handlebars.registerHelper("getter", (Helper<String>) (context, options) -> getter(context));
    }

    public static String getter(String fieldName) {
        return "get" + startWithUpperCase(fieldName) + "()";
    }

    public static String nullableGetter(String fieldName) {
        String orElseMethod = getJavaVersion().isAtLeast(JDK_1_8) ? "orElse(null)" : isFugue() ? "getOrNull()" : "orNull()";
        return "get" + startWithUpperCase(fieldName) + "()." + orElseMethod;
    }

    private static String singular(String text) {
        String singular = text.replaceAll("ies$", "y").replaceAll("s$", "");
        return !singular.equals(text) && !JavaLang.isJavaKeyword(singular) ? singular : text + "Item";
    }

    private static String startWithUpperCase(String text) {
        return text.substring(0, 1).toUpperCase() + text.substring(1);
    }

    public static String eval(String templateName, Object context) {
        try {
            Template template = handlebars.compile("/templates/" + templateName);
            return template.apply(newContext(newContext(GlobalContext.instance), context));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static LanguageLevel getJavaVersion() {
        return PluginUtils.getLanguageLevel(getEvent());
    }

    public static boolean isFugue() {
        return PluginUtils.isClassAvailable(getEvent(), "io.atlassian.fugue.Option");
    }

    public static boolean isGuava() {
        return PluginUtils.isClassAvailable(getEvent(), "com.google.common.base.Optional");
    }

    public static boolean isOptionalTypeAvailable() {
        return PluginUtils.getLanguageLevel(getEvent()).isAtLeast(LanguageLevel.JDK_1_8) || isFugue() || isGuava();
    }

    public static final class GlobalContext {

        public static final GlobalContext instance = new GlobalContext();

        public String getObjectsEquals() {
            return getJavaVersion().isAtLeast(JDK_1_7) ?
                    "java.util.Objects.equals" :
                    "com.google.common.base.Objects.equal";
        }

        public String getObjectsHash() {
            return getJavaVersion().isAtLeast(JDK_1_7) ?
                    "java.util.Objects.hash" :
                    "com.google.common.base.Objects.hashCode";
        }

        public String getOptionalType() {
            return isJava8() ? "java.util.Optional" :
                    isFugue() ? "io.atlassian.fugue.Option" :
                            "com.google.common.base.Optional";
        }

        public String getOptionalConstructor() {
            return isJava8() ? "java.util.Optional.ofNullable" :
                    isFugue() ? "io.atlassian.fugue.Option.option" :
                            "com.google.common.base.Optional.fromNullable";
        }

        public boolean isJava8() {
            return getJavaVersion().isAtLeast(JDK_1_8);
        }

        public boolean isJava7() {
            return getJavaVersion().isAtLeast(JDK_1_7);
        }

        public boolean isSimpleEquals() {
            return isJava7() || isGuava();
        }

        public String getToStringHelper() {
            return PluginUtils.isClassAvailable(getEvent(), "com.google.common.base.MoreObjects") ?
                    "com.google.common.base.MoreObjects.toStringHelper" :
                    "com.google.common.base.Objects.toStringHelper";
        }
    }
}
