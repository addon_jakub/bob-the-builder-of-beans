package com.atlassian.action.model;

import com.atlassian.handlebars.Templates;
import com.atlassian.util.PluginUtils;
import com.google.common.base.Function;
import com.google.common.base.Predicate;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Iterables;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiField;
import com.intellij.psi.PsiType;
import com.intellij.psi.PsiTypeParameter;
import org.jetbrains.annotations.NotNull;

import java.util.Arrays;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.Arrays.asList;

public final class ModelFactory {

    private static final String BUILDER_CLASS_NAME = "Builder";

    private final PsiClass psiClass;
    private final boolean jsonBean;
    private final AnActionEvent event;

    public ModelFactory(PsiClass psiClass, AnActionEvent event) {
        this.psiClass = psiClass;
        this.jsonBean = psiClass.getText().contains("@Json");
        this.event = event;
    }

    public ClassModel createClassModel() {
        final ClassModel.Builder classModel = ClassModel.builder();

        classModel
                .setGenericTypes(getGenericTypes())
                .setName(psiClass.getName())
                .addFields(getNonStaticFields().stream().map(toFieldModel(classModel)).collect(Collectors.toList()))
                .addInheritedFields(getInheritedFields().stream().map(toFieldModel(classModel)).collect(Collectors.toList()))
                .setJsonBean(jsonBean)
                .setIsFinal(psiClass.getModifierList().hasModifierProperty("final"))
                .setHasBuilder(hasStaticInnerBuilderClass());

        if (PluginUtils.isGuavaAvailable(event)) {
            classModel.addAvailableLibrary(ClassModel.Library.GUAVA);
        }

        return classModel.build();
    }

    private boolean hasStaticInnerBuilderClass() {
        return Stream.of(psiClass.getAllInnerClasses()).anyMatch(this::isStaticBuilder);
    }

    private boolean isStaticBuilder(PsiClass psiClass) {
        return isBuilder(psiClass) && isStatic(psiClass);
    }

    private boolean isBuilder(PsiClass psiClass) {
        return BUILDER_CLASS_NAME.equals(psiClass.getName());
    }

    private boolean isStatic(PsiClass psiClass) {
        return psiClass.getModifierList().hasModifierProperty("static");
    }

    @NotNull
    private java.util.function.Function<PsiField, FieldModel> toFieldModel(final ClassModel.Builder classModel) {
        return psiField -> FieldModel.builder()
                .setName(psiField.getName())
                .setType(psiField.getType().getCanonicalText())
                .setIsFinal(psiField.getText().contains("final "))
                .setIsNullable(isNullable(psiField))
                .setIsCollection(isCollection(psiField))
                .setIsOverrides(isOverrides(psiField))
                .setIsDeprecated(isDeprecated(psiField))
                .setJsonPropertyValue(getJsonPropertyValue(psiField))
                .setClassModel(classModel)
                .build();
    }

    private boolean isOverrides(PsiField psiField) {
        return getSupperClasses(psiClass)
                .flatMap(psiClass -> Stream.of(psiClass.getMethods()))
                .anyMatch(method -> method.getName().equals(
                        Templates.getter(psiField.getName()).replaceAll("\\(\\)$", "")));
    }

    private Stream<PsiClass> getSupperClasses(PsiClass psiClass) {
        return "Object".equals(psiClass.getName()) ?
                Stream.empty() :
                Stream.of(psiClass.getSupers())
                        .flatMap(s -> Stream.concat(
                                Stream.of(s),
                                getSupperClasses(s)));
    }

    private String getJsonPropertyValue(PsiField psiField) {
        final Matcher matcher = Pattern.compile("@JsonProperty\\(\"(.+)\"\\)").matcher(psiField.getText());
        if (matcher.find()) {
            return matcher.group(1);
        }

        return psiField.getName();
    }

    private List<String> getGenericTypes() {
        PsiTypeParameter[] typeParameters = psiClass.getTypeParameters();
        return ImmutableList.copyOf(Arrays.stream(typeParameters).map(PsiElement::getText).collect(Collectors.toList()));
    }

    @NotNull
    private List<PsiField> getNonStaticFields() {
        return ImmutableList.copyOf(Arrays.stream(psiClass.getAllFields()).filter(psiField ->
                (psiField.getModifierList() == null || !psiField.getModifierList().hasModifierProperty("static")) &&
                psiClass.equals(psiField.getContainingClass())).collect(Collectors.toList()));
    }

    @NotNull
    private List<PsiField> getInheritedFields() {
        return ImmutableList.copyOf(Arrays.stream(psiClass.getAllFields()).filter(psiField ->
                (psiField.getModifierList() == null || !psiField.getModifierList().hasModifierProperty("static")) &&
                        !psiClass.equals(psiField.getContainingClass())).collect(Collectors.toList()));
    }

    private boolean isCollection(PsiField psiField) {
        return isCollection(psiField.getType());
    }

    private boolean isCollection(PsiType psiType) {
        return psiType.getCanonicalText().startsWith("java.util.Collection") ||
                Stream.of(psiType.getSuperTypes()).anyMatch(this::isCollection);
    }

    private boolean isNullable(PsiField psiField) {
        return jsonBean || Templates.isOptionalTypeAvailable() && psiField.getText().contains("@Nullable");
    }

    private boolean isDeprecated(PsiField psiField) {
        return psiField.getText().contains("@Deprecated");
    }
}
