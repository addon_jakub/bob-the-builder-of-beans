package com.atlassian.action;

import com.atlassian.action.model.ClassModel;
import com.atlassian.action.model.FieldModel;
import com.atlassian.config.BobProperties;
import com.atlassian.handlebars.Templates;
import com.atlassian.util.ClassEditor;
import com.atlassian.util.PluginUtils;
import com.intellij.pom.java.LanguageLevel;

import static com.atlassian.action.BobTheBuilderAction.getBobConfig;
import static com.atlassian.action.BobTheBuilderAction.getEvent;
import static com.atlassian.handlebars.Templates.isOptionalTypeAvailable;

public final class BoilerplateGenerator {
    private final ClassEditor currentClass;
    private final ClassModel classModel;

    public BoilerplateGenerator(final ClassEditor psiClass, final ClassModel classModel) {
        this.currentClass = psiClass;
        this.classModel = classModel;
    }

    public void generate() {
        if (classModel.getFields().isEmpty()) {
            return;
        }

        currentClass.clearClassBodyAfterFieldsDeclaration();

        if (getBobConfig().getProperty(BobProperties.GENERATE_COMMENT)) {
            addComment("generatedByBobComment");
        }

        if (classModel.isJsonBean() && !classModel.getNeedsJsonConstructor()) {
            addMethod("constructorEmpty");
        }

        addMethod("constructor");

        for (FieldModel field : classModel.getFields()) {
            currentClass.addMethod(Templates.eval(classModel.isJsonBean() || !isOptionalTypeAvailable() ? "getter" : "getterWithOption", field));
            if (!field.getIsFinal()) {
                currentClass.addMethod(Templates.eval("setter", field));
            }
        }

        if (needsBuilder()) {
            currentClass.addMethod(Templates.eval("builderStaticFactoryMethod", classModel));
            currentClass.addMethod(Templates.eval("builderFromDataStaticFactoryMethod", classModel));
        }

        addMethod("equals");
        addMethod("hashCode");
        addMethod("toString");

        if (needsBuilder()) {
            currentClass.addClass(Templates.eval("builder", classModel));
        }
    }

    private boolean needsBuilder() {
        return classModel.getNeedsBuilder();
    }

    private void addMethod(String templateName) {
        currentClass.addMethod(Templates.eval(templateName, classModel));
    }

    private void addComment(String templateName) {
        currentClass.addComment(Templates.eval(templateName, classModel));
    }
}
