package com.atlassian.action;

import com.atlassian.action.model.ClassModel;
import com.atlassian.action.model.ModelFactory;
import com.atlassian.config.BobConfig;
import com.atlassian.config.BobConfigFinder;
import com.atlassian.util.ClassEditor;
import com.atlassian.util.PluginUtils;
import com.intellij.openapi.actionSystem.AnAction;
import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.psi.PsiClass;

import java.util.Optional;

import static com.intellij.openapi.command.WriteCommandAction.runWriteCommandAction;

public final class BobTheBuilderAction extends AnAction {

    private static BobConfig bobConfig;
    private static AnActionEvent event;

    /**
     * Gets BobConfig for the class the action is currently being performed on.
     */
    public static BobConfig getBobConfig() {
        return bobConfig;
    }

    /**
     * Returns the current action.
     */
    public static AnActionEvent getEvent() {
        return event;
    }

    @Override
    public void actionPerformed(final AnActionEvent event) {

        BobTheBuilderAction.event = event;

        Optional<ClassEditor> classEditor = PluginUtils.getClass(event);
        if (classEditor.isPresent()) {

            PsiClass psiClass = classEditor.get().psiClass;

            bobConfig = BobConfigFinder.find(psiClass);

            ClassModel classModel = new ModelFactory(psiClass, event).createClassModel();

            final BoilerplateGenerator boilerplateGenerator = new BoilerplateGenerator(classEditor.get(), classModel);

            runWriteCommandAction(psiClass.getProject(), boilerplateGenerator::generate);
        }
    }

    @Override
    public void update(AnActionEvent e) {
        Optional<ClassEditor> currentClass = PluginUtils.getClass(e);
        e.getPresentation().setEnabled(currentClass.isPresent() && !currentClass.get().psiClass.getName().equals("Builder"));
    }
}
