package com.atlassian.util;

import com.google.common.collect.ImmutableSet;

import java.util.Set;

public class JavaLang {
    private static final Set<String> javaKeywords = ImmutableSet.of(
            "abstract", "assert", "boolean", "break", "byte",
            "case", "catch", "char", "class", "const",
            "continue", "default", "do", "double", "else",
            "enum", "extends", "false", "final", "finally",
            "float", "for", "goto", "if", "implements",
            "import", "instanceof", "int", "interface", "long",
            "native", "new", "null", "package", "private",
            "protected", "public", "return", "short", "static",
            "strictfp", "super", "switch", "synchronized", "this",
            "throw", "throws", "transient", "true", "try",
            "void", "volatile", "while"
    );


    public static boolean isJavaKeyword(String word) {
        return javaKeywords.contains(word);
    }

}
