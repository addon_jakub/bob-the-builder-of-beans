package com.atlassian.util;

import com.intellij.openapi.actionSystem.AnActionEvent;
import com.intellij.openapi.actionSystem.LangDataKeys;
import com.intellij.openapi.actionSystem.PlatformDataKeys;
import com.intellij.openapi.editor.Editor;
import com.intellij.openapi.module.Module;
import com.intellij.openapi.project.Project;
import com.intellij.openapi.roots.ModuleRootManager;
import com.intellij.pom.java.LanguageLevel;
import com.intellij.psi.PsiClass;
import com.intellij.psi.PsiElement;
import com.intellij.psi.PsiElementFinder;
import com.intellij.psi.PsiFile;
import com.intellij.psi.impl.PsiElementFinderImpl;
import com.intellij.psi.impl.file.impl.JavaFileManager;
import com.intellij.psi.util.PsiTreeUtil;
import com.intellij.psi.util.PsiUtil;

import java.util.Optional;
import java.util.stream.Stream;

public final class PluginUtils {
    public static LanguageLevel getLanguageLevel(AnActionEvent event) {
        PsiFile file = event.getData(LangDataKeys.PSI_FILE);
        if (file != null) {
            return PsiUtil.getLanguageLevel(file);
        }
        return LanguageLevel.JDK_1_6;
    }

    public static Optional<ClassEditor> getClass(AnActionEvent event) {
        PsiFile file = event.getData(LangDataKeys.PSI_FILE);
        if (file != null) {
            Editor editor = event.getData(PlatformDataKeys.EDITOR);
            if (editor != null) {
                int offset = editor.getCaretModel().getOffset();
                PsiElement elementAt = file.findElementAt(offset);
                PsiClass maybePsiClass = PsiTreeUtil.getParentOfType(elementAt, PsiClass.class);
                if (maybePsiClass != null) {
                    return Optional.of(new ClassEditor(file, maybePsiClass));
                }
            }
        }
        return Optional.empty();
    }

    public static boolean isJarAvailable(AnActionEvent event, String regex) {
        Module module = event.getData(LangDataKeys.MODULE);
        return module != null && Stream
                .of(ModuleRootManager
                        .getInstance(module)
                        .orderEntries()
                        .getClassesRoots())
                .anyMatch(library -> library.getName().toLowerCase().matches(regex));
    }

    public static boolean isGuavaAvailable(AnActionEvent event) {
        return isClassAvailable(event, "com.google.common.collect.ImmutableList");
    }

    public static boolean isClassAvailable(AnActionEvent event, String fullyQualifiedClassName) {
        Optional<ClassEditor> optionalClassEditor = getClass(event);
        Project project = event.getProject();
        if (optionalClassEditor.isPresent() && project != null) {
            PsiElementFinder psiElementFinder = new PsiElementFinderImpl(event.getProject(), JavaFileManager.getInstance(event.getProject()));
            return null != psiElementFinder.findClass(fullyQualifiedClassName, optionalClassEditor.get().psiClass.getResolveScope());
        }
        return false;
    }
}
